package com.iriga.hb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.iriga.hb.utilities.PrefManager;
import com.iriga.hb.utilities.RestEndPoint;

public class BaseActivity extends AppCompatActivity {

    public PrefManager pref;
    public RestEndPoint RestUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = new PrefManager(this);
        RestUrl = new RestEndPoint();
    }
}
