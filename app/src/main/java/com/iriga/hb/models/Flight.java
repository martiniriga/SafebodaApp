package com.iriga.hb.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by marti on 15-Jun-18.
 */

public class Flight implements Parcelable {
    private String DepartureCode;
    private String ArrivalCode;
    private String Arrival;
    private String Departure;
    private String ArrivalTime;
    private String DepartureTime;
    private String Duration;


    public Flight(String mDepartureCode, String mArrivalCode,String mDeparture,String mArrival,
                  String mDepartureTime,String mArrivalTime,String mDuration){
        DepartureCode = mDepartureCode;
        ArrivalCode = mArrivalCode;
        Arrival =mArrival;
        Departure = mDeparture;
        ArrivalTime = mArrivalTime;
        DepartureTime = mDepartureTime;
        Duration = mDuration;
    }

    public static final Creator<Flight> CREATOR = new Creator<Flight>() {
        @Override
        public Flight createFromParcel(Parcel in) {
            return new Flight(in);
        }

        @Override
        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };
    protected Flight(Parcel in) {
        DepartureCode = in.readString();
        ArrivalCode = in.readString();
        Arrival = in.readString();
        Departure = in.readString();
        ArrivalTime = in.readString();
        DepartureTime = in.readString();
        Duration = in.readString();
    }

    public String getDepartureCode() {
        return DepartureCode;
    }

    public void setDepartureCode(String departureCode) {
        DepartureCode = departureCode;
    }

    public String getArrivalCode() {
        return ArrivalCode;
    }

    public void setArrivalCode(String arrivalCode) {
        ArrivalCode = arrivalCode;
    }

    public String getArrival() {
        return Arrival;
    }

    public void setArrival(String arrival) {
        Arrival = arrival;
    }

    public String getDeparture() {
        return Departure;
    }

    public void setDeparture(String departure) {
        Departure = departure;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(DepartureCode);
        parcel.writeString(ArrivalCode);
        parcel.writeString(Departure);
        parcel.writeString(Arrival);
        parcel.writeString(DepartureTime);
        parcel.writeString(ArrivalTime);
        parcel.writeString(Duration);
    }

}
