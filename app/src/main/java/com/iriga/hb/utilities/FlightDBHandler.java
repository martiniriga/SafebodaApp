package com.iriga.hb.utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by marti on 17-Jun-18.
 */

public class FlightDBHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "flight.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_AIRPORTS = "airports";
    public static final String COLUMN_ID = "empId";
    public static final String COLUMN_CODE = "AirportCode";
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_LAT = "Latitude";
    public static final String COLUMN_LONG= "Longitude";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_AIRPORTS + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_CODE + " TEXT, " +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_LAT + " TEXT, " +
                    COLUMN_LONG + " TEXT " +
                    ")";

    public FlightDBHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_AIRPORTS);
        db.execSQL(TABLE_CREATE);
    }


}
