package com.iriga.hb.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.iriga.hb.models.Airport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marti on 17-Jun-18.
 */

public class FlightOperations {
    public static final String LOGTAG = "EMP_MNGMNT_SYS";

        SQLiteOpenHelper dbhandler;
        SQLiteDatabase database;

        private static final String[] allColumns = {
                FlightDBHandler.COLUMN_ID,
                FlightDBHandler.COLUMN_CODE,
                FlightDBHandler.COLUMN_NAME,
                FlightDBHandler.COLUMN_LAT,
                FlightDBHandler.COLUMN_LONG
        };

        public FlightOperations(Context context){
            dbhandler = new FlightDBHandler(context);
        }

        public void open(){
            Log.i(LOGTAG,"Database Opened");
            database = dbhandler.getWritableDatabase();
        }

        public void close(){
            Log.i(LOGTAG, "Database Closed");
            dbhandler.close();

        }

        public Airport addAirport(Airport airport){
            ContentValues values  = new ContentValues();
            values.put(FlightDBHandler.COLUMN_CODE,airport.getCode());
            values.put(FlightDBHandler.COLUMN_NAME,airport.getName());
            values.put(FlightDBHandler.COLUMN_LAT, airport.getLatitude());
            values.put(FlightDBHandler.COLUMN_LONG, airport.getLongitude());           
            long insertid = database.insert(FlightDBHandler.TABLE_AIRPORTS,null,values);
            airport.setId(insertid);
            return airport;

        }

        // Getting single Flight
        public Airport getAirport(long id) {

            Cursor cursor = database.query(FlightDBHandler.TABLE_AIRPORTS,allColumns,FlightDBHandler.COLUMN_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            Airport a = new Airport(Long.parseLong(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4));
            // return Flight
            return a;
        }

    public boolean CheckIsDataAlreadyInDBorNot() {
        String Query = "SELECT count(*) FROM " + FlightDBHandler.TABLE_AIRPORTS;
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        int icount = cursor.getInt(0);
        if(icount>0){
            cursor.close();
            return true;
        }else{
            cursor.close();
            return false;
        }
    }

    public Airport getAirportByCode(String code) {
        Airport a = null;
        Cursor cursor = database.query(FlightDBHandler.TABLE_AIRPORTS,allColumns,FlightDBHandler.COLUMN_CODE + "=?",new String[]{code},null,null, null, null);

        if( cursor != null && cursor.moveToFirst() ){
            a = new Airport(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4));
            // return Flight
            cursor.close();
        }
        return a;
    }

        public List<Airport> getAllAirports() {

            Cursor cursor = database.query(FlightDBHandler.TABLE_AIRPORTS,allColumns,null,null,null, null, null);

            List<Airport> airports = new ArrayList<>();
            if(cursor.getCount() > 0){
                while(cursor.moveToNext()){
                    Airport airport = new Airport();
                    airport.setId(cursor.getLong(cursor.getColumnIndex(FlightDBHandler.COLUMN_ID)));
                    airport.setCode(cursor.getString(cursor.getColumnIndex(FlightDBHandler.COLUMN_CODE)));
                    airport.setName(cursor.getString(cursor.getColumnIndex(FlightDBHandler.COLUMN_NAME)));
                    airport.setLatitude(cursor.getString(cursor.getColumnIndex(FlightDBHandler.COLUMN_LAT)));
                    airport.setLongitude(cursor.getString(cursor.getColumnIndex(FlightDBHandler.COLUMN_LONG)));
                    airports.add(airport);
                }
            }
            // return All Airports
            return airports;
        }



        // Updating Airport
        public int updateAirport(Airport airport) {

            ContentValues values = new ContentValues();
            values.put(FlightDBHandler.COLUMN_CODE, airport.getCode());
            values.put(FlightDBHandler.COLUMN_NAME, airport.getName());
            values.put(FlightDBHandler.COLUMN_LAT, airport.getLatitude());
            values.put(FlightDBHandler.COLUMN_LONG, airport.getLongitude());

            // updating row
            return database.update(FlightDBHandler.TABLE_AIRPORTS, values,
                    FlightDBHandler.COLUMN_ID + "=?",new String[] { String.valueOf(airport.getId())});
        }

        // Deleting Flight
        public void removeFlight(Airport airport) {

            database.delete(FlightDBHandler.TABLE_AIRPORTS, FlightDBHandler.COLUMN_ID + "=" + airport.getId(), null);
        }



}
