package com.iriga.hb.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by marti on 13-Jun-18.
 */

public class RestEndPoint {
    private static String prefixURI ="https://api.lufthansa.com/v1/";
    private String sectionURI;

    public static String URL_AUTH ="oauth/token";
    //param limit: 20-100,optional param limit:recordOffset(integer)
    public static String URL_AIRPORTS = "references/airports?limit=100&lang=en";
//    param airport code
    public static String URL_AIRPORT = "references/airports/%s?lang=en&limit=1";
    //param limit: 20-100, origin,destination optional param:fromDateTime(yyyy-MM-dd[THH:mm]),directFlights(bool)
    //0 = origin, 1 = destination, 2=fromDateTime
    public static String URL_SHEDULE = "operations/schedules/%s/%s/%s?lang=EN&directFlights=%s";


    public void setSectionURI(String sectionURI) {
        this.sectionURI = sectionURI;
    }

    public String getEndPoint() {
        return RestEndPoint.prefixURI + sectionURI;
    }

    public boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setIndeterminate(true);
        pDialog.show();
        return pDialog;
    }

}
