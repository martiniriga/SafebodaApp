package com.iriga.hb.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by marti on 15-Jun-18.
 */

public class PrefManager {
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;
    private static final String PREF_NAME = "UserDetails";
    public static String fontPath ="ProximaNovaAltRegular.ttf";
    public static String token ="token";
    public static String expiryDate ="expiryDate";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public boolean hasToken() {
        return pref.contains(token);
    }

    public String getToken() {
        return pref.getString(token, null);
    }

    public void setToken(String mtoken) {
        editor.putString(token, mtoken);
        editor.commit();
    }

    public boolean hasExpiryDate() {
        return pref.contains(expiryDate);
    }

    public String getExpiryDate() {
        return pref.getString(expiryDate, null);
    }

    public void setExpiryDate(String mexpiryDate) {
        editor.putString(expiryDate, mexpiryDate);
        editor.commit();
    }
}
