package com.iriga.hb;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.iriga.hb.models.Airport;
import com.iriga.hb.utilities.CacheRequest;
import com.iriga.hb.utilities.FlightOperations;
import com.iriga.hb.utilities.RestEndPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FlightSelectionActivity extends BaseActivity {
    private FlightOperations flightOps;
    AutoCompleteTextView txtorigin,txtdestination;
    TextView txtdate;
    Button btnsubmit;
    ArrayAdapter<String> arrayAdapter;

//    String [] airportStringArray;
    ArrayList<String> airportNameArray = new ArrayList<String>();
    ArrayList<String> airportCodeArray = new ArrayList<String>();
    ArrayList<String> latitudeArray = new ArrayList<String>();
    ArrayList<String> longitudeArray = new ArrayList<String>();
    String originAirport= "",destAirport = "",datefrom="" ,directFlights="0";
    Boolean hasFlightlist = false;
    ProgressDialog myDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_selection);
        flightOps = new FlightOperations(this);
        flightOps.open();

        hasFlightlist = flightOps.CheckIsDataAlreadyInDBorNot();
        Log.d("Nextlinkdb",flightOps.getAllAirports().size() + "");
        txtorigin = findViewById(R.id.txtorigin);
        txtdestination = findViewById(R.id.txtdestination);
        txtdate = findViewById(R.id.txtdate);
        LinearLayout dateLayout = findViewById(R.id.dateLayout);
        final CheckBox chkDirectFlight = findViewById(R.id.chkDirectFlight);
        chkDirectFlight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chkDirectFlight.isChecked()){
                    directFlights = "1";
                }else{
                    directFlights = "0";
                }

            }
        });
        dateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                final SimpleDateFormat dformat = new SimpleDateFormat("YYYY-mm-dd", Locale.ENGLISH);

                DatePickerDialog mDatePicker= new DatePickerDialog(FlightSelectionActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Date currentItemDate = new Date();
                        datefrom = padDate(selectedday) + "-" + padDate(selectedmonth + 1) + "-" +selectedyear;
                        txtdate.setText(datefrom);
                        datefrom = selectedyear + "-" + padDate(selectedmonth + 1)+ "-" + padDate(selectedday);
                     }
                },mYear, mMonth, mDay);
                mDatePicker.show();
            }
        });
        btnsubmit = findViewById(R.id.btnsubmit);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateSelection();
            }
        });

        // if token exists fetch flights
        if(pref.hasToken()){
            String tokenExpiryDate = pref.getExpiryDate();
            Calendar calExpiryDate = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            try{
                calExpiryDate.setTime(sdf.parse(tokenExpiryDate));

                if (calExpiryDate.after(now)) { // token still fresh fetch airports
                    if (RestUrl.isNetworkAvailable(this)) {
                        loadAirports(RestEndPoint.URL_AIRPORTS);
                    } else {
                        Toast.makeText(this, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }
                } else { // token expired refresh
                    if (RestUrl.isNetworkAvailable(this)) {
                        getAuthToken();
                    } else {
                        Toast.makeText(this, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        }else{
            if (RestUrl.isNetworkAvailable(this)) {
                getAuthToken();
            } else {
                Toast.makeText(this, getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            }
        }


    }

    private String padDate(int part){
        String partString = Integer.toString(part);
        if(partString.length() == 1){
            partString = "0" + partString;
        }
        return partString;
    }
    //  get Authentication token for all subsequent calls
    private void getAuthToken() {
        RestUrl.setSectionURI(RestEndPoint.URL_AUTH);

        StringRequest getRequest = new StringRequest(Request.Method.POST, RestUrl.getEndPoint(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String token = jsonObject.get("access_token").toString();
                            int expiresIn = jsonObject.getInt("expires_in");
                            pref.setToken(token);
                            Log.d("token", response);
                            Calendar now = Calendar.getInstance();
                            Log.d("timenow", now.getTime().toString());
                            now.add(Calendar.SECOND, expiresIn);
                            String nowPlusExpirySeconds = now.getTime().toString();
                            pref.setExpiryDate(nowPlusExpirySeconds);
                            loadAirports(RestEndPoint.URL_AIRPORTS);
                        } catch ( Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Toast.makeText(FlightSelectionActivity.this,getString(R.string.service_offline), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            public String getBodyContentType(){
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String,String> getParams() throws AuthFailureError{
                Map<String,String> params = new HashMap<>();
                params.put("client_id", "vhskmpa8z37yrrnmwv3sxcfj");
                params.put("client_secret", "paGXAVEGPn");
                params.put("grant_type", "client_credentials");
                return params;
            }

        };
        Volley.newRequestQueue(this).add(getRequest);
    }

    // fetch list of airports
    private void loadAirports(final String url) {
        RestUrl.setSectionURI(url);
        if(url == RestEndPoint.URL_AIRPORTS ) {
            myDialog = RestEndPoint.showProgressDialog(FlightSelectionActivity.this, getString(R.string.gettingReady));
        }
        CacheRequest cacheRequest = new CacheRequest(Request.Method.GET, RestUrl.getEndPoint(),
                new Response.Listener<NetworkResponse>()
                {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(url == RestEndPoint.URL_AIRPORTS) {
                            myDialog.dismiss();
                        }
                        try {
                            final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);
                            JSONArray airportArray = jsonObject.getJSONObject("AirportResource")
                                    .getJSONObject("Airports")
                                    .getJSONArray("Airport");
                            JSONArray linkArray = jsonObject.getJSONObject("AirportResource")
                                    .getJSONObject("Meta")
                                    .getJSONArray("Link");

                            for(int i =0; i< airportArray.length(); i++){
                                JSONObject airportObject = (JSONObject) airportArray.get(i);
                                String code = airportObject.getString("AirportCode");
                                airportCodeArray.add(code);
                                String airportName = airportObject.getJSONObject("Names").getJSONObject("Name").getString("$");
                                airportNameArray.add(airportName);
                                JSONObject Coordinate = airportObject.getJSONObject("Position").getJSONObject("Coordinate");
                                String lat = Coordinate.getString("Latitude");
                                String longit = Coordinate.getString("Longitude");
                                latitudeArray.add(lat);
                                longitudeArray.add(longit);
                            }


                            if(airportNameArray.size() > 0){
                                Log.d("NextlinkArray", airportNameArray.size() + "");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        arrayAdapter = new ArrayAdapter<String>(FlightSelectionActivity.this,
                                                android.R.layout.select_dialog_item, airportNameArray);

                                            //Used to specify minimum number of
                                            //characters the user has to type in order to display the drop down hint.
                                            txtorigin.setThreshold(2);
                                            txtorigin.setAdapter(arrayAdapter);

                                            txtorigin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    originAirport  = parent.getItemAtPosition(position).toString();
                                                }
                                            });
                                            txtdestination.setThreshold(2);
                                            txtdestination.setAdapter(arrayAdapter);
                                            txtdestination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    destAirport  = parent.getItemAtPosition(position).toString();
                                                }
                                            });
                                            arrayAdapter.notifyDataSetChanged();
                                    }
                                });
                                Log.d("Nextlinkurl", url);
                                if(url == "references/airports?limit=100&lang=en&offset=1200"){
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            arrayAdapter.notifyDataSetChanged();
                                            Log.d("Nextlinkother", flightOps.getAllAirports().size() + "");
                                            for (int i= 0; i < airportNameArray.size(); i++){
                                                Airport a = new Airport(airportCodeArray.get(i),
                                                        airportNameArray.get(i),
                                                        latitudeArray.get(i),
                                                        longitudeArray.get(i));
                                                flightOps.addAirport(a);
                                            }
                                            flightOps.close();


                                            //give it enough time to have fetched
                                        }
                                    }, 15000);

                                }

                            }

                            for(int i =0; i< linkArray.length(); i++) {
                                JSONObject linkObject = (JSONObject) linkArray.get(i);
                                String nextUrl = linkObject.getString("@Href")
                                        .replace("https://api.lufthansa.com/v1/", "");
                                if (linkObject.getString("@Rel").equals("next")) { //continue making fetch requests

                                    Log.d("Nextlink", nextUrl);
                                    loadAirports(nextUrl);
                                    return;
                                }
                            }
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(url == RestEndPoint.URL_AIRPORTS) {
                            myDialog.dismiss();
                        }
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode == 401) {
                            // HTTP Status Code: 401 Unauthorized
                            Toast.makeText(FlightSelectionActivity.this,getString(R.string.refreshing_token), Toast.LENGTH_SHORT).show();
                            getAuthToken();
                        }else{
                            Toast.makeText(FlightSelectionActivity.this,getString(R.string.service_offline), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pref.getToken());
                return headers;
            }
        };
        Volley.newRequestQueue(this).add(cacheRequest);
    }

    // validate selection before posting flightselection
    private void validateSelection() {
        if(originAirport == "" ){
            Toast.makeText(this,getString(R.string.please_select_origin),Toast.LENGTH_SHORT).show();
        }else if(destAirport == "" ){
            Toast.makeText(this,getString(R.string.please_select_destination),Toast.LENGTH_SHORT).show();
        }else if(datefrom == "" ){
            Toast.makeText(this,getString(R.string.please_select_date),Toast.LENGTH_SHORT).show();
        }else{
            submitFlightSelection();
        }
    }

    private void submitFlightSelection() {
        Integer index = airportNameArray.indexOf(originAirport);
        String originAirportCode = airportCodeArray.get(index);
        index = airportNameArray.indexOf(destAirport);
        String destAirportCode = airportCodeArray.get(index);

        Intent intent = new Intent(this, FlightScheduleActivity.class);
        intent.putExtra("origin", originAirportCode);
        intent.putExtra("destination", destAirportCode);
        intent.putExtra("datefrom",datefrom);
        intent.putExtra("directFlights",directFlights);
        startActivity(intent);
    }


}
