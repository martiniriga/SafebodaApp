package com.iriga.hb;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.iriga.hb.adapters.FlightAdapter;
import com.iriga.hb.models.Airport;
import com.iriga.hb.models.Flight;
import com.iriga.hb.utilities.FlightOperations;
import com.iriga.hb.utilities.RestEndPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightScheduleActivity extends BaseActivity {
    List<Airport> airportList = new ArrayList<>();
    String origin,destination,datefrom,directFlights="0";
    private boolean loading = true;
    int previousTotal, visibleItemCount, totalItemCount,firstVisibleItem,visibleThreshold;
    RecyclerView rv_schedules;
    TextView empty_view;
    List <Flight> flightList = new ArrayList<>();
    FlightAdapter adapter;
    FlightOperations flightOps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_schedule);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        rv_schedules = findViewById(R.id.rv_schedules);
        empty_view = findViewById(R.id.empty_view);

        origin = getIntent().getStringExtra("origin");
        destination = getIntent().getStringExtra("destination");
        datefrom = getIntent().getStringExtra("datefrom");
        directFlights = getIntent().getStringExtra("directFlights");
        flightOps = new FlightOperations(this);
        flightOps.open();

        final LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        rv_schedules.setLayoutManager(mLayoutManager);
        getFlightSelection();
//        rv_schedules.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                visibleItemCount = rv_schedules.getChildCount();
//                totalItemCount = mLayoutManager.getItemCount();
//                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
//
//                if (loading) {
//                    if (totalItemCount > previousTotal) {
//                        loading = false;
//                        previousTotal = totalItemCount;
//                    }
//                }
//                if (!loading && (totalItemCount - visibleItemCount)
//                        <= (firstVisibleItem + visibleThreshold)) {
//                    // End has been reached
//
//                    Log.d("Yaeye!", "end called");
//
//                    // Do something
//
//                    loading = true;
//                }
//            }
//        });

//       Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
    }

    //get flightselection
    private void getFlightSelection() {

        RestUrl.setSectionURI(String.format(RestEndPoint.URL_SHEDULE, origin, destination,datefrom, directFlights));
        final ProgressDialog myDialog = RestEndPoint.showProgressDialog(this,getString(R.string.gettingFlightSchedule));

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest( RestUrl.getEndPoint(),null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        myDialog.dismiss();
                        try{
                            JSONArray scheduleArray = response.getJSONObject("ScheduleResource").getJSONArray("Schedule");
                            for(int i =0; i< scheduleArray.length(); i++){
                                JSONObject scheduleObject = (JSONObject) scheduleArray.get(i);
                                String duration = scheduleObject.getJSONObject("TotalJourney").getString("Duration");
                                JSONArray flightArray = scheduleObject.getJSONArray("Flight");
                                for (int j = 0; j < flightArray.length(); j++){
                                    JSONObject flightObject = (JSONObject) flightArray.get(i);
                                    JSONObject arrivalObject = flightObject.getJSONObject("Arrival");
                                    JSONObject departureObject = flightObject.getJSONObject("Departure");
                                    String arrivalcode = arrivalObject.getString("AirportCode");
                                    String departurecode = departureObject.getString("AirportCode");
                                    String arrivaltime = arrivalObject.getJSONObject("ScheduledTimeLocal")
                                            .getString("DateTime");
                                    String departuretime = departureObject.getJSONObject("ScheduledTimeLocal")
                                            .getString("DateTime");
                                    String departure = "";
                                    String arrival = "";
                                    if(flightOps.getAirportByCode(arrivalcode) != null) {
                                        Airport arrivalAirport = flightOps.getAirportByCode(arrivalcode);
                                        arrival = arrivalAirport.getName();
                                    }else{
                                        getAirport(arrivalcode);
//                                        Airport arrivalAirport = flightOps.getAirportByCode(arrivalcode);
                                        arrival = arrivalcode;
                                    }
                                    if(flightOps.getAirportByCode(departurecode) != null) {
                                        Airport departureAirport = flightOps.getAirportByCode(departurecode);
                                        departure = departureAirport.getName();
                                    }else{
                                        getAirport(arrivalcode);
//                                        Airport departureAirport = flightOps.getAirportByCode(departurecode);
                                        departure = arrivalcode;
                                    }
                                    Flight flight = new Flight(departurecode,arrivalcode,departure,arrival,departuretime,arrivaltime,duration);
                                    flightList.add(flight);
                                }
                            }
                            flightOps.close();
                            if(flightList.size()>0){
                                adapter = new FlightAdapter(FlightScheduleActivity.this,flightList);
                                rv_schedules.setAdapter(adapter);
                            }else{
                                rv_schedules.setVisibility(View.GONE);
                                empty_view.setVisibility(View.VISIBLE);
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myDialog.dismiss();
                        Toast.makeText(FlightScheduleActivity.this,getString(R.string.failed_to_fetch),Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pref.getToken());
                return headers;
            }
        };
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


    private void getAirport(final String code) {
        RestUrl.setSectionURI(String.format(RestEndPoint.URL_AIRPORT, code));

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest( RestUrl.getEndPoint(),null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            JSONObject airportObject = response.getJSONObject("AirportResource")
                                    .getJSONObject("Airports").getJSONObject("Airport");
                            String airportname = airportObject.getJSONObject("Names").getJSONObject("Name").getString("$");
                            JSONObject coordinateObject = airportObject.getJSONObject("Position").getJSONObject("Coordinate");
                            String latitude = coordinateObject.getString("Latitude");
                            String longitude = coordinateObject.getString("Longitude");
                           Airport airport = new Airport(code,airportname,latitude,longitude);
                           flightOps.open();
                           flightOps.addAirport(airport);

                            flightOps.close();
                        } catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FlightScheduleActivity.this,getString(R.string.failed_to_fetch),Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pref.getToken());
                return headers;
            }
        };
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }
}
