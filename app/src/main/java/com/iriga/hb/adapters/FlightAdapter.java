package com.iriga.hb.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iriga.hb.FlightDetailActivity;
import com.iriga.hb.R;
import com.iriga.hb.models.Flight;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by marti on 17-Jun-18.
 */

public class FlightAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Flight> mFlights;
    private Context mContext;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public FlightAdapter(Context context, List<Flight> flights) { //, final Toolbar toolbar
        mContext = context;
        mFlights = flights;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_row, parent, false);
            return new ItemViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bindData(mFlights.get(position));
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mArrival;
        private TextView mDeparture;
        private TextView mArrivalCode;
        private TextView mDepartureCode;
        private TextView mArrivalTime;
        private TextView mDepartureTime;
        private TextView mDuration;

        private ItemViewHolder(View itemView) {
            super(itemView);

            mArrival = itemView.findViewById(R.id.txtArrival);
            mDeparture = itemView.findViewById(R.id.txtDeparture);
            mArrivalCode= itemView.findViewById(R.id.txtArrivalCode);
            mDepartureCode = itemView.findViewById(R.id.txtDepartureCode);
            mArrivalTime = itemView.findViewById(R.id.txtArrivalTime);
            mDepartureTime = itemView.findViewById(R.id.txtDepartureTime);
            mDuration = itemView.findViewById(R.id.txtDuration);

            itemView.setOnClickListener(this);
        }

        private void bindData(Flight flight) {
            mArrival.setText(flight.getArrival());
            mDeparture.setText(flight.getDeparture());
            mArrivalCode.setText(flight.getArrivalCode());
            mDepartureCode.setText(flight.getDepartureCode());
            mArrivalTime.setText(formatDate(flight.getArrivalTime()));
            mDepartureTime.setText(formatDate(flight.getDepartureTime()));
            mDuration.setText(formatDuration(flight.getDuration()));
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Flight flight = mFlights.get(position);
                Intent intent = new Intent(mContext, FlightDetailActivity.class);
                intent.putExtra(FlightDetailActivity.EXTRA_FLIGHT, flight);
                mContext.startActivity(intent);
            }
        }
    }

    private String formatDate(String datestring){
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
        Date currentItemDate = new Date();
        try {
            currentItemDate = dformat.parse(datestring);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(currentItemDate);
    }

    private String formatDuration(String mduration){
        String duration = mduration.replace("PT","");
        return duration;
    }

    private String getArrivalDate(String datestring, String duration){
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();
        Date currentItemDate = new Date();
        String newDuration = duration.replace("PT","");
        Integer hourIndex = newDuration.indexOf("H");
        Integer minutesIndex = newDuration.indexOf("M");
        String hours = newDuration.substring(0,hourIndex);
        String minutes = newDuration.substring(hourIndex + 1,minutesIndex);


        try {
            currentItemDate = dformat.parse(datestring);
            calendar.setTime(currentItemDate);
            calendar.add(Calendar.HOUR, Integer.parseInt(hours));
            calendar.add(Calendar.MINUTE, Integer.parseInt(minutes));
            currentItemDate = calendar.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(currentItemDate);
    }

    @Override
    public int getItemCount() {
        return (mFlights.size());
    }

}

