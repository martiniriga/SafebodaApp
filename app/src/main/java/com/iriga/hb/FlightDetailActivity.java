package com.iriga.hb;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.iriga.hb.models.Airport;
import com.iriga.hb.models.Flight;
import com.iriga.hb.utilities.FlightOperations;

import org.w3c.dom.Text;

public class FlightDetailActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static String EXTRA_FLIGHT = "flight_detail";
    String origin="",destination="",originLat,originLong,destLat,destLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_detail);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        TextView txtDepartureTime = findViewById(R.id.txtDepartureTime);
        TextView txtArrivalTime = findViewById(R.id.txtArrivalTime);
        TextView txtDuration = findViewById(R.id.txtDuration);
        TextView txtArrivalCode = findViewById(R.id.txtArrivalCode);
        TextView txtDepartureCode = findViewById(R.id.txtDepartureCode);
        TextView txtDeparture = findViewById(R.id.txtDeparture);
        TextView txtArrival = findViewById(R.id.txtArrival);
        FlightOperations flightOperations = new FlightOperations(this);
        flightOperations.open();
        Flight flight = getIntent().getParcelableExtra(EXTRA_FLIGHT);
        txtDepartureTime.setText(flight.getDepartureTime());
        txtArrivalTime.setText(flight.getArrivalTime());
        txtDuration.setText(flight.getDuration());
        txtArrivalCode.setText(flight.getArrivalCode());
        txtDepartureCode.setText(flight.getDepartureCode());
        Airport arrivalAirport = flightOperations.getAirportByCode(flight.getArrivalCode());
        if(arrivalAirport != null){
            origin = arrivalAirport.getName();
            originLat = arrivalAirport.getLatitude();
            originLong = arrivalAirport.getLongitude();
        }
        txtDeparture.setText(origin);

        Airport departureAirport = flightOperations.getAirportByCode(flight.getDepartureCode());
        if(departureAirport != null){
            destination = departureAirport.getName();
            destLat = departureAirport.getLatitude();
            destLong = departureAirport.getLongitude();
        }
        txtArrival.setText(destination);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Origin Position
        LatLng originPosition = new LatLng(Double.parseDouble(originLat), Double.parseDouble(originLong));
        LatLng destPosition = new LatLng(Double.parseDouble(destLat), Double.parseDouble(destLong));
        mMap.addMarker(new MarkerOptions()
                .position(originPosition)
                .title(origin)
                .icon(bitmapDescriptorFromVector(FlightDetailActivity.this, R.drawable.ic_flight_takeoff)));


        mMap.addPolyline(new PolylineOptions().add(destPosition).width(8).color(Color.RED));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(originPosition));

    }

    //since map only accepts bitmap
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
