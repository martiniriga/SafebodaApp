package com.iriga.hb;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.iriga.hb.utilities.PrefManager;
import com.iriga.hb.utilities.RestEndPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashscreenActivity extends AppCompatActivity {
    static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company*/
            @Override
            public void run() {
                    Intent i = new Intent(SplashscreenActivity.this, FlightSelectionActivity.class);
                    startActivity(i);
                    finish();

            }
        }, SPLASH_TIME_OUT);
    }

}
